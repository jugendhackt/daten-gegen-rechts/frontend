import axios from "axios";
import dayjs from "dayjs";
import "dayjs/locale/de";

dayjs.locale('de');

let url = process.env.VUE_APP_ROOT_API + '/api/v1/incidents';
let loaded = false
let loadpromise = null
let crimedata = []
let sortedmonthcounts = []

function* monthIterator(start, end) {
  let year = parseInt(start.substr(0,4))
  let month = parseInt(start.substr(5,2))

  const twoDigit = a => a < 10 ? '0' + a : a

  let curDateStr = ''

  while (curDateStr <= end) {
    curDateStr = year + '-' + twoDigit(month)
    yield curDateStr
		month ++
		if (month >= 12) {
			year ++
			month -= 12
    }
  }
}

function sortAndFillDateObject(obj) {
  let keysSorted = Object.keys(obj).sort()
  keysSorted = keysSorted.filter(k => !(k.startsWith("19")));
  let sobj = {}
  let filled = Array.from(monthIterator(keysSorted[0], keysSorted[keysSorted.length-1]))
  for (let key of filled) {
    if (key in obj) {
      sobj[key] = obj[key]
    } else {
      sobj[key] = 0
    }
  }
  return sobj
}

async function loadData() {
  if (loadpromise) {
    return loadpromise
  }
  loadpromise = axios.get(url).then(response => response.data)
    .then(apidata => {
      let monthcounts = {}
      apidata.forEach(entry => {
        entry.time = dayjs(entry.timestamp).format('[am] D. MMM YYYY [um] H:mm [Uhr]');
        let month = entry.timestamp.substr(0,7)
        if (month in monthcounts) {
          monthcounts[month] += 1
        } else {
          monthcounts[month] = 1
        }
        crimedata.push(entry);
      });
      sortedmonthcounts = sortAndFillDateObject(monthcounts);
  }).then(() => loaded = true);
  return loadpromise
}

export default {
  async monthcounts() {
    if (!loaded) {
      await loadData();
    }
    console.log(sortedmonthcounts);
    return sortedmonthcounts;
  },
  async crimedata() {
    if (!loaded) {
      await loadData();
    }
    console.log(crimedata);
    return crimedata;
  }
}
